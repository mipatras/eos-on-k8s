#! /bin/bash

# **************************************************************************** #
#                                  Entrypoint                                  #
# **************************************************************************** #


if [ "$#" -lt 1 ]; then
    echo "ERROR: Illegal number of parameters. Syntax: $0 <k8s-namespace>..."
    exit 1
fi


# @todo Deal with the deletion of the default namespace (forbidden)
for NAMESPACE in "$@"; do
	kubectl delete ns $NAMESPACE
	rm -rf "./eos-instance-${NAMESPACE}"
done
kubectl config set-context $(kubectl config current-context) --namespace="default"

#! /bin/bash


sed -i 's/eos-mgm-test.eoscluster.cern.ch/eos-mgm1.eos-mgm1.%%%NAMESPACE%%%.svc.cluster.local/g' /etc/sysconfig/eos;
sed -i 's/eos-mq-test.eoscluster.cern.ch/eos-mq.%%%NAMESPACE%%%.svc.cluster.local/g' /etc/sysconfig/eos;

sed -i 's/eos-krb-test.eoscluster.cern.ch/eos-kdc.%%%NAMESPACE%%%.svc.cluster.local/g' /etc/krb5.conf;
sed -i 's/eos-mgm-test.eoscluster.cern.ch/eos-mgm1.eos-mgm1.%%%NAMESPACE%%%.svc.cluster.local/g' /etc/eos/fuse.conf;

# fuse 
sed -i 's/eostest.cern.ch/eos-mgm1.eos-mgm1.%%%NAMESPACE%%%.svc.cluster.local/g' /etc/sysconfig/eos;

# krb
sed -i 's/eos-mgm-test.eoscluster.cern.ch/eos-mgm1.eos-mgm1.%%%NAMESPACE%%%.svc.cluster.local/g' /kdc.sh;

# mgm
sed -i 's/eos-qdb-test.eoscluster.cern.ch/eos-qdb.%%%NAMESPACE%%%.svc.cluster.local/g' /etc/xrd.cf.mgm;

# fst
sed -i 's/eos-mgm-test.eoscluster.cern.ch/eos-mgm1.eos-mgm1.%%%NAMESPACE%%%.svc.cluster.local/g' /eos_fst_setup.sh;
sed -i 's/eos-mq-test.eoscluster.cern.ch/eos-mq.%%%NAMESPACE%%%.svc.cluster.local/g' /etc/xrd.cf.fst;


# set the geotag and instance name (optional)
# sed -i 's/docker/kuber/g' /etc/sysconfig/eos;
# sed -i 's/docker/kuber/g' /eos_mgm_fs_setup.sh;

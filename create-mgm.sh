#! /bin/bash

# Regular Colors
Color_Off='\033[0m'       # Text Reset
Black='\033[0;30m'        # Black
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Yellow='\033[0;33m'       # Yellow
Blue='\033[0;34m'         # Blue
Purple='\033[0;35m'       # Purple
Cyan='\033[0;36m'         # Cyan
White='\033[0;37m'        # White

# **************************************************************************** #
#                                  utilities                                  #
# **************************************************************************** #

function usage () {	
    echo "Usage: $0 [-q] [-i <eos_image_tag>] -n <k8s_namespace>"
    exit 1
}

function gen_role () {
	role=$1; role_count=$2;
	[ $role == "cli" ] && image_tag=$EOS_CLI_IMAGE_TAG || image_tag=$EOS_IMAGE_TAG
	if [[ ! $role_count ]]; then
		cp "$TEMPLATES_DIR/eos-${1}.template.yaml" "eos-${role}.yaml"
		sed -i "s/%%%NAMESPACE%%%/${NAMESPACE}/g" "eos-${role}.yaml"
		sed -i "s/%%%IMAGE_TAG%%%/${image_tag}/g" "eos-${role}.yaml"
	else
		for i in $(seq 1 $role_count); do 
			cp "$TEMPLATES_DIR/eos-${1}.template.yaml" "eos-${role}${i}.yaml"
			sed -i "s/%%%NAMESPACE%%%/${NAMESPACE}/g" "eos-${role}${i}.yaml"
			sed -i "s/%%%IMAGE_TAG%%%/${image_tag}/g" "eos-${role}${i}.yaml"
			sed -i "s/%%%NUMBER%%%/${i}/g" "eos-${role}${i}.yaml"
		done
	fi
}

function get_podname () {	
	kubectl get pods --namespace=${NAMESPACE} --no-headers -o custom-columns=":metadata.name" -l app=$1
	# kubectl get pods -l app=$1 -o=name | sed "s/^.\{4\}//" 
}


# **************************************************************************** #
#                                  Entrypoint                                  #
# **************************************************************************** #


NAMESPACE="ns$(date +%s)"
EOS_IMAGE_TAG="4.4.38"
with_qdb=0
MGM_COUNT=1


while getopts 'n:i:q' opt; do
	case "${opt}" in
        n) # a DNS-1123 label must consist of lower case alphanumeric characters or '-', and must start and end with an alphanumeric character
			if [[ $OPTARG =~ ^[a-z0-9]([-a-z0-9]*[a-z0-9])?$ ]]; 
				then NAMESPACE=${OPTARG}
				else echo "! Wrong arg -$opt"; usage
			fi ;;
        i)
            EOS_IMAGE_TAG=${OPTARG} ;;
        q)
            with_qdb=1 ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

echo "NAMESPACE=$NAMESPACE"
echo "EOS_IMAGE_TAG=$EOS_IMAGE_TAG"
echo "with_qdb=$with_qdb"



# ********************************************************************************************** 
# Generation of the K8s manifests and configuration scripts for a complete namespaced instance #

TEMPLATES_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/templates" # one-liner that gives the full directory name of the script no matter where it is being called from.
EOS_INSTANCE_DIR="eos-instance-$NAMESPACE"
mkdir $EOS_INSTANCE_DIR
cd $EOS_INSTANCE_DIR


# Generate the namespace
cp "$TEMPLATES_DIR/namespace.template.yaml" "namespace-$NAMESPACE.yaml"
sed -i "s/%%%NAMESPACE%%%/$NAMESPACE/g" "namespace-$NAMESPACE.yaml"

# Generate the manifests
if [[ $with_qdb == 1 ]]; then 
	gen_role qdb
fi
gen_role mgm $MGM_COUNT




# Generate the container configuration script
cp "$TEMPLATES_DIR/configure-container.template.sh" "configure-container-$NAMESPACE.sh"
chmod +x "configure-container-$NAMESPACE.sh"
sed -i "s/%%%NAMESPACE%%%/${NAMESPACE}/g" "configure-container-${NAMESPACE}.sh"
if [[ $with_qdb == 1 ]]; then # when running in QuarkDB mode we need to change the namespace library loaded by the MGM
	echo "sed -i 's/libEosNsInMemory/libEosNsQuarkdb/g' /etc/xrd.cf.mgm;" >> "configure-container-${NAMESPACE}.sh"
	echo -e "${Yellow} var 'with_qdb' set to 1, EOS will run in QuarkDB mode (use libEosNsQuarkdb.so)${Color_Off}"
fi



# ********************************************************************************************** 
# Deploy the instance #

shopt -s nullglob # The shopt -s nullglob will make the glob expand to nothing if there are no matches.
all_yaml=(eos-*.yaml)
all_app=( "${all_yaml[@]%.yaml}" ) #remove file extension


# Create the namespace
kubectl create -f "namespace-$NAMESPACE.yaml"


# Check for proper cluster state and then create the instance
while [[ $(kubectl get pods --namespace=$NAMESPACE) == "No resources found." ]]; do
	echo 'waiting for the cluster to be "fresh" ...'
	sleep 3
done
for i in ${all_yaml[@]}; do
	kubectl create -f $i
done
count=0
while [[ $count -le 120 ]] && [[ ! `kubectl get pods --namespace=$NAMESPACE | grep Running | wc -l` -eq ${#all_yaml[@]} ]]; do # @todo improve the counting 
	echo -ne "\r Waiting $count secs for all the pods to be Running... `kubectl get pods --namespace=$NAMESPACE | grep Running | wc -l`" / "${#all_yaml[@]}"
	count=$(($count + 1))
	sleep 1;
done
if [[ $count -lt 120 ]]; then
	echo -e "${Green} OK, all the Pods are in Running state! `kubectl get pods --namespace=$NAMESPACE | grep Running | wc -l`" / "${#all_yaml[@]} ${Color_Off}"
else
	echo -e "${Red} KO, not all the Pods are in Running state! `kubectl get pods --namespace=$NAMESPACE | grep Running | wc -l`" / "${#all_yaml[@]} ${Color_Off}"
	exit 1
fi

# Live Pods (containers) configuration
for item in ${all_app[@]}; do 
	echo "configuring live $item ..."
	kubectl cp configure-container-$NAMESPACE.sh $NAMESPACE/$(get_podname $item):/configure-container-$NAMESPACE.sh
	kubectl exec $(get_podname $item) --namespace=$NAMESPACE -- /bin/bash -c "./configure-container-$NAMESPACE.sh"
done
# @todo add error handling
echo -e "${Green} All app configured ${Color_Off}"



# # ********************************************************************************************** 
# # Start the EOS services in each Pod

echo "Starting the EOS services in each Pod"

if [[ $with_qdb == 1 ]]; then 
	echo -e "${Yellow} Exec on eos-qdb... ${Color_Off}"
	kubectl exec $(get_podname eos-qdb) --namespace=${NAMESPACE} -- /bin/bash -c "./eos_qdb_setup.sh"
fi


# @todo add error handling
echo -e "${Green} Exec went OK for all the Pods ${Color_Off}"



# tmp
echo ""
echo "NAMESPACE=$NAMESPACE"
echo "EOS_IMAGE_TAG=$EOS_IMAGE_TAG"
echo "with_qdb=$with_qdb"

#! /bin/bash

# Regular Colors
Color_Off='\033[0m'       # Text Reset
Black='\033[0;30m'        # Black
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Yellow='\033[0;33m'       # Yellow
Blue='\033[0;34m'         # Blue
Purple='\033[0;35m'       # Purple
Cyan='\033[0;36m'         # Cyan
White='\033[0;37m'        # White

# **************************************************************************** #
#                                  utilities                                  #
# **************************************************************************** #

function usage () {	
    echo "Usage: $0 [-q] [-m <eos-mgm_count>] [-i <eos_image_tag>] [-u <eos_client_image_tag>] [-c <eos-cli_count>] [-f <eos-fst_count>] -n <k8s_namespace>"
    exit 1
}

function gen_role () {
	role=$1; role_count=$2;
	[ $role == "cli" ] && image_tag=$EOS_CLI_IMAGE_TAG || image_tag=$EOS_IMAGE_TAG
	if [[ ! $role_count ]]; then
		cp "$TEMPLATES_DIR/eos-${1}.template.yaml" "eos-${role}.yaml"
		sed -i "s/%%%NAMESPACE%%%/${NAMESPACE}/g" "eos-${role}.yaml"
		sed -i "s/%%%IMAGE_TAG%%%/${image_tag}/g" "eos-${role}.yaml"
	else
		for i in $(seq 1 $role_count); do 
			cp "$TEMPLATES_DIR/eos-${1}.template.yaml" "eos-${role}${i}.yaml"
			sed -i "s/%%%NAMESPACE%%%/${NAMESPACE}/g" "eos-${role}${i}.yaml"
			sed -i "s/%%%IMAGE_TAG%%%/${image_tag}/g" "eos-${role}${i}.yaml"
			sed -i "s/%%%NUMBER%%%/${i}/g" "eos-${role}${i}.yaml"
		done
	fi
}

function get_podname () {	
	kubectl get pods --namespace=${NAMESPACE} --no-headers -o custom-columns=":metadata.name" -l app=$1
	# kubectl get pods -l app=$1 -o=name | sed "s/^.\{4\}//" 
}


# **************************************************************************** #
#                                  Entrypoint                                  #
# **************************************************************************** #


NAMESPACE="ns$(date +%s)"
EOS_IMAGE_TAG="4.4.38"
EOS_CLI_IMAGE_TAG="4.4.38"
CLI_COUNT=1
FST_COUNT=7
with_qdb=0
MGM_COUNT=1


while getopts 'n:i:u:c:f:qm:' opt; do
	case "${opt}" in
        n) # a DNS-1123 label must consist of lower case alphanumeric characters or '-', and must start and end with an alphanumeric character
			if [[ $OPTARG =~ ^[a-z0-9]([-a-z0-9]*[a-z0-9])?$ ]]; 
				then NAMESPACE=${OPTARG}
				else echo "! Wrong arg -$opt"; usage
			fi ;;
        i)
            EOS_IMAGE_TAG=${OPTARG} ;;
        u)
            EOS_CLI_IMAGE_TAG=${OPTARG} ;;
        c) # clients count must be an integer greater than 0
			if [[ $OPTARG =~ ^[0-9]+$ ]] && [[ $OPTARG -gt 0 ]]; 
				then CLI_COUNT=${OPTARG}
				else echo "! Wrong arg -$opt"; usage
			fi ;;
        f) # fsts count must be an integer greater than 0
			if [[ $OPTARG =~ ^[0-9]+$ ]] && [[ $OPTARG -gt 0 ]]; 
				then FST_COUNT=${OPTARG}
				else echo "! Wrong arg -$opt"; usage
			fi ;;
        q)
            with_qdb=1 ;;
        m) # mgms count must be an integer greater than 0
			if [[ $OPTARG =~ ^[0-9]+$ ]] && [[ $OPTARG -gt 0 ]]; 
				then MGM_COUNT=${OPTARG}
				else echo "! Wrong arg -$opt"; usage
			fi ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

echo "NAMESPACE=$NAMESPACE"
echo "EOS_IMAGE_TAG=$EOS_IMAGE_TAG"
echo "EOS_CLI_IMAGE_TAG=$EOS_CLI_IMAGE_TAG"
echo "CLI_COUNT=$CLI_COUNT"
echo "FST_COUNT=$FST_COUNT"
echo "with_qdb=$with_qdb"
echo "MGM_COUNT=$MGM_COUNT"



# ********************************************************************************************** 
# Generation of the K8s manifests and configuration scripts for a complete namespaced instance #

TEMPLATES_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/templates" # one-liner that gives the full directory name of the script no matter where it is being called from.
EOS_INSTANCE_DIR="eos-instance-$NAMESPACE"
mkdir $EOS_INSTANCE_DIR
cd $EOS_INSTANCE_DIR


# Generate the namespace
cp "$TEMPLATES_DIR/namespace.template.yaml" "namespace-$NAMESPACE.yaml"
sed -i "s/%%%NAMESPACE%%%/$NAMESPACE/g" "namespace-$NAMESPACE.yaml"

# Generate the manifests
gen_role kdc
gen_role mq
gen_role fst $FST_COUNT
gen_role cli $CLI_COUNT
if [[ $with_qdb == 1 ]]; then 
	gen_role qdb
fi
gen_role mgm $MGM_COUNT

# Generate the container configuration script
cp "$TEMPLATES_DIR/configure-container.template.sh" "configure-container-$NAMESPACE.sh"
chmod +x "configure-container-$NAMESPACE.sh"
sed -i "s/%%%NAMESPACE%%%/${NAMESPACE}/g" "configure-container-${NAMESPACE}.sh"
if [[ $with_qdb == 1 ]]; then # when running in QuarkDB mode we need to change the namespace library loaded by the MGM
	echo "sed -i 's/libEosNsInMemory/libEosNsQuarkdb/g' /etc/xrd.cf.mgm;" >> "configure-container-${NAMESPACE}.sh"
	echo -e "${Yellow} var 'with_qdb' set to 1, EOS will run in QuarkDB mode (use libEosNsQuarkdb.so)${Color_Off}"
fi


# ********************************************************************************************** 
# Deploy the instance #

shopt -s nullglob # The shopt -s nullglob will make the glob expand to nothing if there are no matches.
all_yaml=(eos-*.yaml)
all_app=( "${all_yaml[@]%.yaml}" ) #remove file extension


# Create the namespace
kubectl create -f "namespace-$NAMESPACE.yaml"


# Check for proper cluster state and then create the instance
while [[ $(kubectl get pods --namespace=$NAMESPACE) == "No resources found." ]]; do
	echo 'waiting for the cluster to be "fresh" ...'
	sleep 3
done
for i in ${all_yaml[@]}; do
	kubectl create -f $i
done
count=0
while [[ $count -le 120 ]] && [[ ! `kubectl get pods --namespace=$NAMESPACE | grep Running | wc -l` -eq ${#all_yaml[@]} ]]; do # @todo improve the counting 
	echo -ne "\r Waiting $count secs for all the pods to be Running... `kubectl get pods --namespace=$NAMESPACE | grep Running | wc -l`" / "${#all_yaml[@]}"
	count=$(($count + 1))
	sleep 1;
done
if [[ $count -lt 120 ]]; then
	echo -e "${Green} OK, all the Pods are in Running state! `kubectl get pods --namespace=$NAMESPACE | grep Running | wc -l`" / "${#all_yaml[@]} ${Color_Off}"
else
	echo -e "${Red} KO, not all the Pods are in Running state! `kubectl get pods --namespace=$NAMESPACE | grep Running | wc -l`" / "${#all_yaml[@]} ${Color_Off}"
	exit 1
fi

# Live Pods (containers) configuration
for item in ${all_app[@]}; do 
	echo "configuring live $item ..."
	kubectl cp configure-container-$NAMESPACE.sh $NAMESPACE/$(get_podname $item):/configure-container-$NAMESPACE.sh
	kubectl exec $(get_podname $item) --namespace=$NAMESPACE -- /bin/bash -c "./configure-container-$NAMESPACE.sh"
done
if [[ $MGM_COUNT -gt 1 ]]; then
	echo -e "${Yellow} var 'MGM_COUNT' greater than 1, env var EOS_USE_QDB_MASTER will be set${Color_Off}"
	echo "echo 'export EOS_USE_QDB_MASTER=1' >> /etc/sysconfig/eos" >> "configure-container-$NAMESPACE.sh"
	for i in $(seq 1 $MGM_COUNT); do 
		echo "re-configuring live eos-mgm${i} ..."
		kubectl cp configure-container-$NAMESPACE.sh $NAMESPACE/$(get_podname eos-mgm${i}):/configure-container-$NAMESPACE.sh
		kubectl exec $(get_podname eos-mgm${i}) --namespace=$NAMESPACE -- /bin/bash -c "sed -i 's/eos-mgm1/eos-mgm${i}/g' \"configure-container-$NAMESPACE.sh\";"
		kubectl exec $(get_podname eos-mgm${i}) --namespace=$NAMESPACE -- /bin/bash -c "./configure-container-$NAMESPACE.sh"
	done
fi
# @todo add error handling
echo -e "${Green} All app configured ${Color_Off}"



# ********************************************************************************************** 
# Start the EOS services in each Pod

echo "Starting the EOS services in each Pod"


echo -e "${Yellow} Exec on eos-kdc... ${Color_Off}"
kubectl exec $(get_podname eos-kdc) --namespace=$NAMESPACE -- /bin/bash -c "./kdc.sh"
until kubectl exec $(get_podname eos-kdc) --namespace=$NAMESPACE -- /bin/bash -c "[ -f /root/eos.keytab ]" ; do
	kubectl exec $(get_podname eos-kdc) --namespace=$NAMESPACE -- /bin/bash -c "./kdc.sh"
done;
# cp specific keytab from kdc to mgms and clients
kubectl cp $NAMESPACE/$(get_podname eos-kdc):/root/eos.keytab keytab.tmp
for i in $(seq 1 $MGM_COUNT); do 
	kubectl cp keytab.tmp $NAMESPACE/$(get_podname eos-mgm${i}):/etc/eos.krb5.keytab
done
rm -f keytab.tmp
kubectl cp $NAMESPACE/$(get_podname eos-kdc):/root/admin1.keytab keytab.tmp
for i in $(seq 1 $CLI_COUNT); do 
	kubectl cp keytab.tmp  ${NAMESPACE}/$(get_podname eos-cli1):/root/admin1.keytab
done
rm -f keytab.tmp

echo -e "${Yellow} Exec on eos-mq... ${Color_Off}"
kubectl exec $(get_podname eos-mq) --namespace=${NAMESPACE} -- /bin/bash -c "./eos_mq_setup.sh"

if [[ $with_qdb == 1 ]]; then 
	echo -e "${Yellow} Exec on eos-qdb... ${Color_Off}"
	kubectl exec $(get_podname eos-qdb) --namespace=${NAMESPACE} -- /bin/bash -c "./eos_qdb_setup.sh"
fi

echo -e "${Yellow} Exec on eos-mgms... ${Color_Off}"
for i in $(seq 1 $MGM_COUNT); do 
	kubectl exec $(get_podname eos-mgm${i}) --namespace=${NAMESPACE} -- /bin/bash -c "./eos_mgm_setup.sh"
done

echo -e "${Yellow} Exec on every eos-fst... ${Color_Off}"
failure=0; pids="";
for i in $(seq 1 $FST_COUNT); do 
	kubectl exec $(get_podname "eos-fst${i}") --namespace=${NAMESPACE} -- /bin/bash -c "./eos_fst_setup.sh ${i}" &
    pids="${pids} $!"
    sleep 0.1
done
for pid in ${pids}; do
  wait ${pid} || let "failure=1"
done
if [ "${failure}" == "1" ]; then
	echo -e "${Red} Failed to Exec on one of the FSTs ${Color_Off}"
  	exit 1
fi

echo -e "${Yellow} Exec on eos-mgms (fs setup)... ${Color_Off}"
for i in $(seq 1 $MGM_COUNT); do 
	kubectl exec $(get_podname eos-mgm${i}) --namespace=${NAMESPACE} -- /bin/bash -c "./eos_mgm_fs_setup.sh ${FST_COUNT}"
	# if [[ ! `kubectl exec $(get_podname eos-mgm1) --namespace=${NAMESPACE} -- /bin/bash -c "./eos_mgm_fs_setup.sh ${FST_COUNT}"` ]]; then exit 1; fi
done

echo -e "${Yellow} Exec on every eos-cli... ${Color_Off}"
for i in $(seq 1 $CLI_COUNT); do 
	kubectl exec $(get_podname "eos-cli${i}") --namespace=${NAMESPACE} -- /bin/bash -c "kinit -kt /root/admin1.keytab admin1@TEST.EOS"
	kubectl exec $(get_podname "eos-cli${i}") --namespace=${NAMESPACE} -- /bin/bash -c "kvno host/eos-mgm1.eos-mgm1.$NAMESPACE.svc.cluster.local"
done

# @todo add error handling
echo -e "${Green} Exec went OK for all the Pods ${Color_Off}"


# Check status
kubectl exec $(kubectl get pods --namespace=${NAMESPACE} --no-headers -o custom-columns=":metadata.name" -l app=eos-mgm1) --namespace=${NAMESPACE} -- eos fs ls
for i in $(seq 1 $CLI_COUNT); do 
	kubectl exec $(kubectl get pods --namespace=${NAMESPACE} --no-headers -o custom-columns=":metadata.name" -l app=eos-cli1) --namespace=${NAMESPACE} -- /bin/bash -c "eos whoami;"
done


# tmp
echo ""
echo "EOS_IMAGE_TAG=$EOS_IMAGE_TAG"
echo "EOS_CLI_IMAGE_TAG=$EOS_CLI_IMAGE_TAG"
echo "CLI_COUNT=$CLI_COUNT"
echo "FST_COUNT=$FST_COUNT"
echo "NAMESPACE=$NAMESPACE"
echo "with_qdb=$with_qdb"
echo "MGM_COUNT=$MGM_COUNT"
